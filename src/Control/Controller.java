package Control;

import View.SoftwareFrame;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;

import Model.BankAccount;
import View.SoftwareFrame;

public class Controller {
	private static final double INITIAL_BALANCE = 1000;
	public BankAccount account;
	public SoftwareFrame Sframe;
	
	public static void main(String[] arg){
		new Controller();
	}
	
	public Controller() {
		this.account = new BankAccount(INITIAL_BALANCE);
		this.Sframe = new SoftwareFrame();
		ActionListener listener = new AddInterestListener();
		this.Sframe.setListener(listener);
	}
	
	class AddInterestListener implements ActionListener{
		public void actionPerformed(ActionEvent event){
			double rate = Sframe.getRate();
	        double interest = account.calInterest(rate);
	        account.deposit(interest);
	        Sframe.setText("" + account.getBalance());
		}       
	}
	
	//ActionListener list;
/**���͡��� Object �ͧ  BankAccount ��� SoftwareFrame �� attribute 
 * ������� public BankAccount account ��� public SoftwareFrame Sframe 
 * �繡�����¡�� class �ͧ����ͧ  �֧��ͧ��С�ȡ�����¡�� ������� attribute class 
 * �ͧ bankAccount ��� SoftwareFrame ���¡�� ��**/
 
}