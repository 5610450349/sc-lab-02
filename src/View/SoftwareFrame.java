package View;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import Model.BankAccount;

public class SoftwareFrame extends JFrame{

	private static final double DEFAULT_RATE = 5;
	
	private JLabel showRateLabel;
	private JTextField showRateField;
	private JButton showButton;
	private JLabel showResultLabel;
	private BankAccount account;

	//public static void main(String[] args){
		//new SoftwareFrame();
	//}
	
	public SoftwareFrame() {
		account = new BankAccount(1000);
		showResultLabel = new JLabel("  balance : "+ account.getBalance());
		
		createButton();
		createTextField();
		createFrame();
		
		setVisible(true);
		setSize(450, 100);
		setResizable(true);
	}
	
	//public void setRateLabel(String str) {
		//showRateLabel.setText(str);
	//}

	private void createTextField(){
	    showRateLabel = new JLabel("  Interest Rate : ");

	    final int FIELD_WIDTH = 10;
	    showRateField = new JTextField(FIELD_WIDTH);
	    showRateField.setText("" + DEFAULT_RATE);
	}
	
	private void createFrame() {
		setLayout(new GridLayout(1,4));
		
		add(this.showRateLabel);
		add(this.showRateField);
		add(this.showButton);
		add(this.showResultLabel);
	}
	
	public double getRate(){
		double rate = Double.parseDouble(showRateField.getText());
		return rate;
	}
	private void createButton(){
		showButton = new JButton("Add Interest");
	}
	
	public void setText(String s){
		showResultLabel.setText("balance : " + s);
	}

	public void setListener(ActionListener listener) {
		showButton.addActionListener(listener);
	}
	   
	
	

}

